//
// Created by Nikolay Yakovets on 2018-02-01.
//

#ifndef QS_SIMPLEESTIMATOR_H
#define QS_SIMPLEESTIMATOR_H

#include "SimpleEvaluator.h"
#include "Estimator.h"
#include "SimpleGraph.h"
#include <map>

class SimpleEstimator : public Estimator {

    std::shared_ptr<SimpleGraph> graph;

public:
    explicit SimpleEstimator(std::shared_ptr<SimpleGraph> &g);
    ~SimpleEstimator() = default;

    void prepare() override ;
    cardStat estimate(RPQTree *q) override ;

private:
    std::map<int,cardStat> oneGram;
    std::map<std::pair<int,int>,int> twoGram;

};


#endif //QS_SIMPLEESTIMATOR_H

