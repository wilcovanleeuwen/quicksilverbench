//
// Created by Nikolay Yakovets on 2018-02-01.
//

#include <SimpleEvaluator.h>
#include <set>
#include "SimpleGraph.h"
#include "SimpleEstimator.h"

SimpleEstimator::SimpleEstimator(std::shared_ptr<SimpleGraph> &g){

    // works only with SimpleGraph
    graph = g;

    oneGram = std::map<int,cardStat>();
}

void SimpleEstimator::prepare() {

    // do your prep here
    SimpleEvaluator eval = SimpleEvaluator(graph);

    for (int label=0; label<(int)graph->getNoLabels(); label++) {
        std::shared_ptr<SimpleGraph> l1Graph = eval.project((uint32_t)label,false,graph);
        cardStat l1CardStat = SimpleEvaluator::computeStats(l1Graph);
//        std::cout << "label" << label <<" (" << l1CardStat.noOut << "," << l1CardStat.noPaths << "," << l1CardStat.noIn << ")" << std::endl;
        std::pair<int,cardStat> insertPair (label,l1CardStat);
        oneGram.insert(insertPair);
    }
}


cardStat SimpleEstimator::estimate(RPQTree *q) {

    // perform your estimation here
    if (q->isLeaf()) {
        std::regex directLabel(R"((\d+)\+)");
        std::regex inverseLabel(R"((\d+)\-)");

        std::smatch matches;

        uint32_t label;
        bool inverse;

        if (std::regex_search(q->data, matches, directLabel)) {
            label = (uint32_t)std::stoi(matches[1]);
            inverse = false;
        } else if (std::regex_search(q->data, matches, inverseLabel)) {
            label = (uint32_t)std::stoi(matches[1]);
            inverse = true;
        } else {
            std::cerr << "Label parsing failed!" << std::endl;
            return cardStat {0, 0, 0};
        }

        if (inverse) {
            auto stat = oneGram.find(label)->second;
            return cardStat{stat.noIn, stat.noPaths, stat.noOut};
        } else {
            return oneGram.find(label)->second;
        }
    } else {
        cardStat left = estimate(q->left);
        cardStat right = estimate(q->right);

        double sizeJ = (double) std::min(left.noIn, right.noOut);
        double avgIn = (double) left.noPaths / (double) left.noIn;
        double avgOut = (double) right.noPaths / (double) right.noOut;
        uint32_t size = (uint32_t) (sizeJ * avgIn * avgOut);

        uint32_t newOut;
        uint32_t newIn;

        bool leftIncl = false;
        if (left.noIn < right.noOut) {
            leftIncl = true;
        }

        if (leftIncl) {
            newOut = left.noOut;
            newIn = (uint32_t)((double) right.noIn * ((double)left.noIn / (double)right.noOut));
        } else {
            newOut = (uint32_t)((double) left.noOut * ((double) right.noOut / (double) left.noIn));
            newIn = right.noIn;
        }
        return cardStat{newOut,size,newIn};
    }
}
